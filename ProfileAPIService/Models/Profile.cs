﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProfileAPIService.Controllers
{
    public class Profile
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string description { get; private set; }
    }
}
